var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/wi');
var db = mongoose.connection;

db.once('open', function callback() {
    console.log('MongoDB connected! ' + 'db name: ' + db.name);
});

module.exports = mongoose;