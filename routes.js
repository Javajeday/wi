/**
 * Created by nmozharov on 21.01.17.
 */

let getPointController = require('./api/get_point.controller');
let setPointController = require('./api/set_point.controller');
let deletePointController = require('./api/delete_point.controller');

let routes = {
    getPoint: {
        method: 'GET',
        url: '/points/point',
        ctrl: getPointController
    },
    setPoint: {
        method: 'POST',
        url: '/points/point',
        ctrl: setPointController
    },
    deletePoint: {
        method: 'DELETE',
        url: '/points/point',
        ctrl: deletePointController
    },
};

module.exports = routes;