let Point = require('../models/point');
let async = require('async');

let controller = function (req, res) {
    async.waterfall([
            function(callback) {
                Point.findOne({}, function (err, point) {
                    if (err) throw new Error(err);
                    if (point) {
                        callback(null, point)
                    } else {
                        callback(null, false);
                    }
                });
            }
        ],
        function (err, point) {
            if (err) throw new Error(err);
            if (point) {
                res.json(point);
            } else {
                res.status(404).json({errorMessage: 'Not found'});
            }
        })
};

module.exports = controller;