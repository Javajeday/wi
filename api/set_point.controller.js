let Point = require('../models/point');
let async = require('async');

let controller = function (req, res) {
    async.series([
        function (callback) {
            Point.findOne({}, function (err, point) {
                if (err) throw new Error(err);
                if (point) {
                    console.log('updated')
                    Point.findOneAndUpdate({ '_id': point._id },
                        { image: req.body.image, points: req.body.points },
                        function (err, updatedPoint) {
                            if (err) throw new Error(err);

                            if (updatedPoint) {
                                callback()
                            } else {
                                res.status(500).json({errorMessage: 'Internal server error'});
                            }
                        })
                } else {
                    console.log('created')
                    Point.create({
                        image: req.body.image,
                        points: req.body.points

                    }, function (err, point) {
                        if (err) throw new Error(err);

                        if (point) {
                            callback();
                        } else {
                            res.status(500).json({errorMessage: 'Internal server error'});
                        }
                    });
                }
            });
        }
    ], function (err) {
        if (err) throw new Error(err);

        res.json({ updated: true });
    });
};

module.exports = controller;