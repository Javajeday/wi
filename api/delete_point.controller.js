let Point = require('../models/point');
let async = require('async');

let controller = function (req, res) {
    async.series([
        function (callback) {
            Point.findOneAndRemove({}, function (err, removedPoint) {
                if (err) throw new Error(err);
                if (removedPoint) {
                    callback()
                } else {
                    res.status(404).json({errorMessage: 'Not found'});
                }
            });
        }
    ], function (err) {
        if (err) throw new Error(err);

        res.json({ removed: true });
    });
};

module.exports = controller;