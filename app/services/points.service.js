/**
 * Created by nmozharov on 21.01.17.
 */

(function() {
    'use strict';

    angular
        .module('app')
        .service('Points', Points);

    function Points($http) {
        let defaultPoints = [
            {
                id: 1,
                ssid: 'AP1',
                signal: {
                    radius: 20,
                    strength: 10
                },
                top: 10,
                left: 30

            },
            {
                id: 2,
                ssid: 'AP2',
                signal: {
                    radius: 30,
                    strength: 12
                },
                top: 52,
                left: 40

            },
            {
                id: 3,
                ssid: 'Home',
                signal: {
                    radius: 24,
                    strength: 20
                },
                top: 50,
                left: 25

            },
            {
                id: 4,
                ssid: 'FreeWifi',
                signal: {
                    radius: 35,
                    strength: 20
                },
                top: 25,
                left: 60

            },
            {
                id: 5,
                ssid: 'HiddenPoint',
                signal: {
                    radius: 40,
                    strength: 16
                },
                top: 80,
                left: 90

            }
        ];

        return {
            default: defaultPoints,
            get: function() {
                return $http.get('/points/point')
            },
            set: function(data) {
                return $http.post('/points/point', data)
            },
            delete: function() {
                return $http.delete('/points/point')
            }
        }
    }
}());