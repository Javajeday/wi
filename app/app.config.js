/**
 * Created by nmozharov on 21.01.17.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .config(config)
        .directive('ngDraggable', draggable)
        .directive('tooltip', tooltip)
        .directive('fileUpload', fileUpload);

    function config($stateProvider, $urlRouterProvider, $compileProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                controller: 'homeController',
                templateUrl: 'home.tpl.html'
            })
            .state('map', {
                url: '/map',
                controller: 'mapController',
                templateUrl: 'map.tpl.html'
            });

        $urlRouterProvider.otherwise('/home');
    }

    function draggable($document) {
        return {
            restrict: 'A',
            scope: {
                dragOptions: '=ngDraggable'
            },
            link: function (scope, elem, attr) {
                let startX, startY, x = 0, y = 0,
                    start, stop, drag, container,
                    debounceMove = debounce(mousemove, 100);

                let width = elem[0].offsetWidth,
                    height = elem[0].offsetHeight;

                // Obtain drag options
                if (scope.dragOptions) {
                    start = scope.dragOptions.start;
                    drag = scope.dragOptions.drag;
                    stop = scope.dragOptions.stop;
                    var selector = scope.dragOptions.container;
                    container = document.querySelector(selector);
                }

                elem.on('mousedown', function (e) {
                    e.preventDefault();
                    startX = e.clientX - elem[0].offsetLeft;
                    startY = e.clientY - elem[0].offsetTop;
                    if (scope.$parent.$parent.draggable) {
                        document.addEventListener('mousemove', mousemove);
                        document.addEventListener('mouseup', mouseup);
                    }
                    if (start) start(e);
                });

                // Handle drag event
                function mousemove(e) {
                    y = e.clientY - startY;
                    x = e.clientX - startX;
                    setPosition();
                    if (drag) drag(e);
                }

                // Unbind drag events
                function mouseup(e) {
                    document.removeEventListener('mousemove', mousemove);
                    document.removeEventListener('mouseup', mouseup);
                    if (stop) stop(e);
                }

                function setPosition() {
                    if (x < 0) {
                        x = 0;
                    } else if (x > container.offsetWidth) {
                        x = container.offsetWidth - width;
                    }
                    if (y < 0) {
                        y = 0;
                    } else if (y > container.offsetHeight - height) {
                        y = container.offsetHeight - height;
                    }
                    elem.css({
                        top: intToPercents().y + '%',
                        left: intToPercents().x  + '%'
                    });
                }

                function intToPercents() {
                    return {
                        x: Math.round(x / (container.offsetWidth / 100)),
                        y: Math.round(y / (container.offsetHeight / 100))
                    }
                }
            }
        }
    }

    function tooltip() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                attrs.$observe('title', () => {
                    $(element).tooltip('destroy');
                    $(element).tooltip();
                });
                $(element).hover(function(){
                    $(element).tooltip('show');
                }, function(){
                    $(element).tooltip('hide');
                });
            }
        };
    }

    function fileUpload() {
        return {
            scope: true,
            link: function(scope, el, attrs) {
                el.bind('change', function(event) {
                    scope.$emit("loading", {});
                    var files = event.target.files;
                    for (var i = 0; i < files.length; i++) {
                        //emit event upward
                        scope.$emit("fileSelected", { file: files[i] });
                    }
                });
            }
        };
    }

    function debounce(func, wait, immediate) {
        let timeout;
        return function() {
            let context = this, args = arguments;
            let later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };
}());