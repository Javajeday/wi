/**
 * Created by nmozharov on 21.01.17.
 */

(function() {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'ui.bootstrap',
            'ui.bootstrap.tpls',
            'ngAnimate',
        ])
}());