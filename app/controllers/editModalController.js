(function () {
    angular.module('app')
        .controller('editModalController', editModalController);
    function editModalController($scope, $http, $uibModalInstance, $timeout, data) {
        $scope.newName = '';
        $scope.close = function() {
            $uibModalInstance.close({result: false})
        };

        $scope.save = function() {
            $uibModalInstance.close({result: $scope.newName})
        };
    }
})();