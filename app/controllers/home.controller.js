/**
 * Created by nmozharov on 21.01.17.
 */

(function () {
    'use strict';

    angular.module('app')
        .controller('homeController', homeController);

    function homeController($scope, $http, $interval, $timeout, $uibModal, Points) {
        let imageContainer = document.querySelector('.map-container');
        let imageNode = document.querySelector('.map-image');
        let heatInstances = [];

        $scope.draggable = false;
        $scope.editMode = false;
        $scope.heatMap = false;
        $scope.files = [];
        $scope.points = [];
        $scope.loadedFile = null;

        $scope.$on('$viewContentLoaded', function(){
            imageContainer.classList.add('loaded');
            adjustContainerDimensions();
        });

        window.addEventListener('resize', adjustContainerDimensions);
        function adjustContainerDimensions() {
            // remove canvas on resize for better visual quality
            $scope.destroyHeatMap();
            $timeout(function() {
                let imageAspect = imageNode.width / imageNode.height;

                imageContainer.style.width = '100%';
                let computedWidth = parseInt(window.getComputedStyle(imageContainer)['width']);
                imageContainer.style.height = (computedWidth) / imageAspect + 'px';
            }, 0)
        }

        // start/drag/stop callback available
        $scope.dragOptions = {
            draggable: $scope.draggable,
            container: '.map-container'
        };

        $scope.selectedPoints = [];
        $scope.selectPoint = function($index) {
            if (!$scope.editMode) return;
            if ($scope.selectedPoints.indexOf($index) === -1) {
                $scope.selectedPoints.push($index);
                $scope.points[$index].selected = true;
                for (let point in $scope.points) {
                    $scope.points[point].lastselected = false;
                }
                $scope.points[$index].lastselected = true;
            } else {
                let indexToRemove = $scope.selectedPoints.indexOf($index);
                $scope.points[$index].selected = false;
                $scope.selectedPoints.splice(indexToRemove, 1);
                for (let point in $scope.points) {
                    $scope.points[point].lastselected = false;
                }
                if ($scope.selectedPoints.length) {
                    $scope.points[$scope.selectedPoints[$scope.selectedPoints.length - 1]].lastselected = true;
                }
            }
        };

        $scope.openEditModal = function ($event) {
            $event.stopPropagation();
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'editmodal.tpl.html',
                controller: 'editModalController',
                size: 'xs',
                backdrop: 'static',
                resolve: {
                    data: function () {}
                }
            });

            modalInstance.result.then(function (result) {
                let newNAme = result.result;
                if (newNAme) {
                    $scope.points.forEach((point) => {
                        if (point.selected) {
                            point.ssid = newNAme;
                        }
                    })
                }
            });
        };

        $scope.toggleDraggable = function() {
            $scope.draggable = !$scope.draggable;
            $scope.editMode = false;
            $scope.heatMap = false;
            $scope.dropSelection();
            $scope.destroyHeatMap();
        };

        $scope.toggleEditMode = function() {
            $scope.editMode = !$scope.editMode;
            $scope.draggable = false;
            $scope.heatMap = false;
            $scope.dropSelection();
            $scope.destroyHeatMap();
        };

        $scope.toggleHeatMap = function() {
            $scope.heatMap = !$scope.heatMap;
            $scope.editMode = false;
            $scope.draggable = false;
            $scope.dropSelection();
            $scope.drawHeatMap();
        };

        $scope.drawHeatMap = function () {
            if (heatInstances.length) {
                $scope.destroyHeatMap();
                return;
            }
            let radiusRatio = Math.round(parseInt(window.getComputedStyle(imageContainer)['width'])) / 4;
            $scope.points.forEach((point) => {
                let pointNode = document.querySelector(`[data-id="${point.id}"]`);
                let posX = Math.round(parseInt(window.getComputedStyle(pointNode)['left']));
                let posY = Math.round(parseInt(window.getComputedStyle(pointNode)['top']));
                let widthDisplace = Math.round(parseInt(window.getComputedStyle(pointNode)['width'])) / 2;
                let heightDisplace = Math.round(parseInt(window.getComputedStyle(pointNode)['height'])) / 2;
                let dataPoint = {
                    x: posX + widthDisplace,
                    y: posY + heightDisplace,
                    value: point.signal.strength + 30
                };
                let config = {
                    container: imageContainer,
                    radius: point.signal.radius + radiusRatio,
                    maxOpacity: .5,
                    minOpacity: 0,
                    blur: .75
                };
                let heatInstance = h337.create(config);
                heatInstance.addData(dataPoint);
                heatInstances.push(heatInstance);
            });
        };

        $scope.destroyHeatMap = function() {
            $scope.heatMap = false;
            heatInstances.forEach((instance, index) => {
                let canvas = instance._renderer.canvas;
                $(canvas).remove();
                instance = null;
            });
            heatInstances= [];
        };

        $scope.dropSelection = function() {
            for (let point in $scope.points) {
                $scope.points[point].lastselected = false;
                $scope.points[point].selected = false;
            }
        };

        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                $scope.files = [];
                $scope.points = [];
                $scope.loadedFile = null;
                if ($scope.files.length < 2) {
                    if (args.file.type == 'image/jpeg' || args.file.type == 'image/jpg' || args.file.type == 'image/png') {
                        $scope.files.push(args.file);
                        let fileReader = new FileReader();
                        fileReader.readAsDataURL($scope.files[0]);
                        fileReader.onload = function(e) {
                            $timeout(function() {
                                $(imageContainer).css("background", "url('" + e.target.result + "')  no-repeat scroll 50% 50% / contain padding-box border-box");
                                imageNode.src = e.target.result;
                                $timeout(function(){
                                    $scope.loading = false;
                                }, 500);
                                $scope.points = Points.default;
                                $scope.loadedFile = e.target.result;
                                adjustContainerDimensions();
                            });
                        }
                    }
                }
            });
        });

        $scope.$on("loading", function (event, args) {
            $scope.loading = true;
        });

        $scope.setTip = function(event) {
             $scope.tipData = event.target.dataset.tip;
        };

        $scope.getPoint = function() {
            Points.get().then((data) => {
                $scope.loadedFile = data.data.image;
                $scope.points = data.data.points;
                $scope.files.push(1);
                imageNode.src = data.data.image;
                $(imageContainer).css("background", "url('" + data.data.image + "')  no-repeat scroll 50% 50% / contain padding-box border-box");
                adjustContainerDimensions();
            }, (data) => {
                // handle maybe?
            })
        };

        $scope.setPoint = function() {
            $scope.updatePoints();
            $scope.points.forEach((point, index) => {
                delete point.selected;
                delete point.lastselected;
            });
            Points.set({ image: $scope.loadedFile, points: $scope.points })
                .then(
                    function(response){
                        $scope.tipData = 'Изменения сохранены'
                    },
                    function(response){
                        // handle maybe?
                    }
                );
        };

        $scope.deletePoint = function() {
            $scope.points = [];
            $scope.files = [];
            $scope.loadedFile = null;
            imageNode.src = '';
            Points.delete()
                .then(
                    function(response){
                        // handle maybe too
                    },
                    function(response){
                        // handle maybe too
                    }
                );
        };

        $scope.updatePoints = function() {
            let pointsNodeArray = Array.from(document.querySelectorAll('.map-wifi-logo'));
            pointsNodeArray.forEach((updPoint) => {
                $scope.points.forEach((oldPoint, index) => {
                    if (oldPoint.id == updPoint.dataset.id) {
                        $scope.points[index].top = parseInt(updPoint.style.top);
                        $scope.points[index].left = parseInt(updPoint.style.left);
                    }
                })
            })
        };
        $scope.getPoint();
    }
})();