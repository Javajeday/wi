/**
 * Created by nmozharov on 21.01.17.
 */
'use strict';

var gulp = require('gulp'),
    concatjs = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    less = require('gulp-less');

gulp.task('scripts', function() {
    return gulp.src([
        'node_modules/heatmap.js/build/heatmap.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/angular/angular.js',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        'node_modules/angular-ui-router/release/angular-ui-router.js',
        'node_modules/angular-animate/angular-animate.js',
        'app/app.js',
        'app/app.config.js',
        'app/services/points.service.js',
        'app/controllers/editModalController.js',
        'app/controllers/home.controller.js',
        'app/controllers/map.controller.js',
        // 'app/directives/draggable.js'
    ])
        .pipe(concatjs('main.js'))
        .pipe(gulp.dest('dist/'));
});

gulp.task('copyfonts', function() {
    gulp.src('node_modules/font-awesome/fonts/**/*.{ttf,woff,woff2,eof,svg}')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('less', function () {
    return gulp.src([
        'node_modules/font-awesome/less/font-awesome.less',
        'node_modules/bootstrap/less/bootstrap.less',
        'app/assets/*.less',
    ])
        .pipe(less())
        .pipe(concatCss("main.css"))
        .pipe(gulp.dest('dist/'));
});