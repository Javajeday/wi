/**
 * Created by nmozharov on 21.01.17.
 */
'use strict';

let http = require('http'),
    express = require('express'),
    routes = require("./routes"),
    app = express(),
    serverPort = 80,
    body = require('body-parser'),
    mongodb = require('mongodb'),
    mongoose = require('./dbconfig'),
    args = require('yargs').argv,
    devmode = typeof args['devmode'] !== 'undefined';

app.use(body.json({limit: '5mb'}));
app.use(body.json());
// static in dev mode only & nginx must be up
app.use(express.static('dist'));
app.use(express.static('static'));
app.use(express.static('app/templates'));

for (let route in routes) {
    if (routes[route].method == 'GET') {
        app.get(routes[route].url, routes[route].ctrl)
    }
    if (routes[route].method == 'POST') {
        app.post(routes[route].url, routes[route].ctrl)
    }
    if (routes[route].method == 'DELETE') {
        app.delete(routes[route].url, routes[route].ctrl)
    }
}

/**
 * node -v 5.12.0/6.2.2 tested successfully
 * npm -v 3.8.6
 * --use-strict flag needed for ES6 features
 */
http.createServer(app).listen(serverPort, () => {
    console.log(`Server running on port: ${serverPort}`)
});