let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let pointSchema = new Schema({
    image: { type: String, required: true },
    points: { type: Array, default: [] }
});

var Point = mongoose.model('Point', pointSchema);

module.exports = Point;